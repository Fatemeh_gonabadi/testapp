package com.example.testapp.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.example.testapp.R;
import com.example.testapp.adapter.RecyclerAdapter;
import com.example.testapp.remote.api.ApiInterface;
import com.example.testapp.remote.api.ApiClient;
import com.example.testapp.remote.pojo.Profile;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    RecyclerAdapter recyclerAdapter;
    public static final String BASE_URL = "https://run.mocky.io/v3/";
    ProgressBar spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intView();
        setUpRv();
        generateData();
    }

    public void intView() {
        recyclerView = findViewById(R.id.recyclerview);
        spinner = findViewById(R.id.action_bar_spinner);
    }

    private void setUpRv() {
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerAdapter = new RecyclerAdapter(this);
        recyclerView.setAdapter(recyclerAdapter);
    }

    private void generateData() {
        ApiInterface apiInterface = ApiClient.get().create(ApiInterface.class);
        Call<List<Profile>> call = apiInterface.getProduct();
        spinner.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<List<Profile>>() {

            @Override
            public void onResponse(Call<List<Profile>> call, Response<List<Profile>> response) {
                spinner.setVisibility(View.INVISIBLE);
                if (response.isSuccessful()) {
                    List<Profile> listItem = response.body();
                    recyclerAdapter.setDataList(listItem);
                } else {
                    alertDialog();
                }
            }

            @Override
            public void onFailure(Call<List<Profile>> call, Throwable t) {
                spinner.setVisibility(View.INVISIBLE);
                alertDialog();
            }
        });
    }

    public void alertDialog() {
        android.app.AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
        builder1.setMessage(R.string.not_responding);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Yes",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        generateData();
                    }
                });
        builder1.setNegativeButton(
                "No",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        AlertDialog alert11 = builder1.create();
        alert11.show();
    }
}

