package com.example.testapp.remote.api;

import com.example.testapp.remote.pojo.Profile;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("6198b4d5-7a8c-4637-badf-f6a9b553b812")
    Call<List<Profile>> getProduct();
}
