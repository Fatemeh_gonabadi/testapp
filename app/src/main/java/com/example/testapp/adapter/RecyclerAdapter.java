package com.example.testapp.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.testapp.R;
import com.example.testapp.remote.pojo.Profile;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {
    private Context context;
    private List<Profile> dataList = new ArrayList<>();

    public RecyclerAdapter(Context context) {
        this.context = context;
    }

    public void setDataList(List<Profile> List) {
        this.dataList = List;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.profile_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Profile profileItem = dataList.get(position);
        holder.init(profileItem, position);
    }

    @Override
    public int getItemCount() {
        if (dataList != null) {
            return dataList.size();
        }
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name, ssn;
        ImageView profile, delete;

        public MyViewHolder(View itemView) {
            super(itemView);
        }

        public void init(final Profile item, final int position) {
            name = itemView.findViewById(R.id.tv_name);
            ssn = itemView.findViewById(R.id.tv_ssn);
            profile = itemView.findViewById(R.id.iv_profile);
            delete = itemView.findViewById(R.id.iv_delete);
            name.setText(item.getName());
            ssn.setText(item.getNationalCode());
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            setAlertDialog(v);
        }

        public void setAlertDialog(View v) {
            android.app.AlertDialog.Builder builder1 = new AlertDialog.Builder(v.getRootView().getContext());
            builder1.setMessage(R.string.delete_row);
            builder1.setTitle(R.string.warning);
            builder1.setCancelable(true);
            builder1.setPositiveButton(
                    "Yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            removeItem(getAdapterPosition());
                        }
                    });
            builder1.setNegativeButton(
                    "No",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }

        public void removeItem(int position) {
            dataList.remove(position);
            notifyItemRemoved(position);
        }
    }
}
